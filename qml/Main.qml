/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import QtSystemInfo 5.5
import QtWebEngine 1.8

import Example 1.0

ApplicationWindow  {
    id: window
    width: 360
    height: 520
    visible: true
    title: "DiabloWeb"

    StackView {
        id: stackView
        anchors.fill: parent
    }

    Component.onCompleted: {
        stackView.push("PlayerPage.qml")
        Example.createDiabloFolder()
        Example.createDownloadFolder()
        Suru.theme = Suru.Dark
    }

    ScreenSaver {
        screenSaverEnabled: !(Qt.application.active)
  }
}
