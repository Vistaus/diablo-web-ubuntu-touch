/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import QtWebEngine 1.8

import Example 1.0

Page  {
    anchors.fill: parent
    WebEngineView {
        id: webView
        anchors.fill: parent
        url: "https://d07riv.github.io/diabloweb/"

        settings.accelerated2dCanvasEnabled: true

        profile: WebEngineProfile {
            httpUserAgent: "Mozilla/5.0 (Linux; Android 11; SM-A125U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Mobile Safari/537.36"
            storageName: "Storage"
            persistentStoragePath: "/home/phablet/.local/share/diabloweb.nitanmarcel/QWebEngine"

            onDownloadRequested: (download) => {
                var filename = download.path.replace(/^.*[\\\/]/, '')
                download.path = "/home/phablet/.local/share/diabloweb.nitanmarcel/diablo/saves/" + filename
                download.accept()
            }

            onDownloadFinished: (download) => {
                stackView.push("DownloadPage.qml", {url: download.path})
            }
        }

        onNewViewRequested : function (request) {
                    request.action = WebEngineNavigationRequest.IgnoreRequest
                    Qt.openUrlExternally(request.requestedUrl)
        }


        onFileDialogRequested: (request) => {
            request.accepted = true;
            if (request.acceptedMimeTypes[0] === ".mpq") {
                if (Example.gameDataExists())
                    request.dialogAccept("/home/phablet/.local/share/diabloweb.nitanmarcel/diablo/DIABDAT.MPQ")
                else {
                    request.dialogReject()
                    gameDialog.open()
                }
            }

            else {
                var uploadPage = stackView.push("UploadPage.qml")
                uploadPage.imported.connect(function (fileUrl) {
                    request.dialogAccept(fileUrl);
                })
                uploadPage.cancel.connect(() => request.dialogReject())
            }
        }

        onJavaScriptDialogRequested: (request) => {
            request.accepted = true;
            jsDialog.message = request.message
            jsDialog.accepted.connect(() => request.dialogAccept())
            jsDialog.rejected.connect(() => request.dialogReject())
            jsDialog.open()
        }

        onLoadingChanged: {
            if (loading === false) {
                window.showFullScreen(true)
            }
        } 
    }

    Dialog {
        id: gameDialog

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        parent: ApplicationWindow.overlay

        modal: true
        title: "DIABDAT.MPQ not found"
        //title: 
        standardButtons: Dialog.Ok

        Label {
            text: "Place \"DIABDAT.MPQ\" in /home/phablet/.local/share/diabloweb.nitanmarcel/diablo (create directory if doesn't exists) and press \"Select MPQ\" again."
            wrapMode: Text.WordWrap
            width: parent.width
        }
    }

        Dialog {
        id: jsDialog
        property string message;

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        parent: ApplicationWindow.overlay

        modal: true
        title: message
        standardButtons: Dialog.Yes | Dialog.No
    }


    Rectangle {
        id: webViewLoadingIndicator
        anchors.fill: parent
        z: 1
        color: Suru.backgroundColor
        visible: webView.loading === true

        BusyIndicator {
            id: busy
            anchors.centerIn: parent
        }
    }
}
